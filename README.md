# README #

Used to display in a table, the VisualForce Page View Count (introduced in Summer '17)

[https://releasenotes.docs.salesforce.com/en-us/summer17/release-notes/rn_vf_metrics.htm](https://releasenotes.docs.salesforce.com/en-us/summer17/release-notes/rn_vf_metrics.htm)